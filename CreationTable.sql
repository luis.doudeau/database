DROP TABLE IF EXISTS RESERVER;
DROP TABLE IF EXISTS OCCUPANT;
DROP TABLE IF EXISTS SALLE;
DROP TABLE IF EXISTS DATES;
DROP TABLE IF EXISTS TYPES; 


CREATE TABLE TYPES(
    idT Decimal(4),
    nomType Varchar(20),
    PRIMARY KEY (idT)
);

CREATE TABLE SALLE(
    idS Decimal(4),
    nomS Varchar(20),
    capacite Decimal(4),
    PRIMARY KEY (idS)
);

CREATE TABLE OCCUPANT(
    idO Decimal(4),
    nomO Varchar(25),
    caracteristique Varchar(15),
    idT Decimal(4),
    PRIMARY KEY (idO)
);

CREATE TABLE DATES(
    idDate Decimal(4),
    dates date,
    PRIMARY KEY(idDate)
);

CREATE TABLE RESERVER(
    idS Decimal(4),
    idO Decimal(4),
    idDate Decimal(4),
    heureDeb date,
    duree time,
    nbPersonnes int,    
    PRIMARY KEY (idS, idO, idDate)
);


ALTER TABLE RESERVER ADD FOREIGN KEY (idS) REFERENCES SALLE (idS);
ALTER TABLE RESERVER ADD FOREIGN KEY (idO) REFERENCES OCCUPANT (idO);
ALTER TABLE RESERVER ADD FOREIGN KEY (idDate) REFERENCES DATES (idDate);
ALTER TABLE OCCUPANT ADD FOREIGN KEY (idT) REFERENCES TYPES (idT);
