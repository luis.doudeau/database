-- TP 2
-- Nom: DOUDEAU  , Prenom: Luis

-- Il y a dans les 4 grandes catégories :
    -- Les requêtes du type au moins un
    -- Les requêtes du type tous
    -- Les requêtes du type jamais/aucun
    -- Les requêtes du type au moin deux

-- +------------------+--
-- * Question 1 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les panels dont ne fait pas partie Louane DJARA?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | France global 1 |
-- | Moins de 50 ans |
-- +-----------------+
-- = Reponse question 1.

--type aucun/jamais 

select distinct nompan from PANEL where nompan not in (select nompan from CONSTITUER Natural join SONDE Natural join PANEL where nomsond = 'DJARA' and prenomsond = 'Louane'); 

-- +------------------+--
-- * Question 2 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les prénoms de sondé commençant par un A qui n'apparaissent pas dans la tranche d'age 20-29 ans? Classez ces noms par ordre alphabétique.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+
-- | prenomSond |
-- +------------+
-- | Alice      |
-- | Allan      |
-- | Amaury     |
-- | Ambre      |
-- | Anaïs      |
-- | etc...
-- = Reponse question 2.

-- type jamais/aucun 

select distinct prenomsond from SONDE where SUBSTR(prenomsond, 1, 1) = 'A' and prenomsond not in (select prenomsond from SONDE natural join CARACTERISTIQUE natural join TRANCHE where valdebut > 19 and valfin < 30 ORDER BY prenomsond ASC); 

-- +------------------+--
-- * Question 3 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--   Quels sont les panels dont tous les sondés ont moins de 60 ans? Rappel: CURDATE() donne la date du jour et DATEDIFF(d1,d2) donne le nombre de jours entre d1 et d2.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-----------------+
-- | nomPan          |
-- +-----------------+
-- | Moins de 50 ans |
-- +-----------------+
-- = Reponse question 3.

-- type tous

select distinct nompan from PANEL where nompan not in (select distinct nompan from SONDE natural join CARACTERISTIQUE natural join TRANCHE natural join CONSTITUER natural join PANEL where (2022 - YEAR(datenaissond)) >= 60);

-- +------------------+--
-- * Question 4 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quelles sont les catégories qui comportent des personnes nées en 1976? On rappelle que YEAR(d) donne l'année de la date d sous la forme d'un entier.

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +-------------------------------------------------+
-- | intituleCat                                     |
-- +-------------------------------------------------+
-- | Cadres, professions intellectuelles supérieures |
-- | Professions intermédiaires                      |
-- | Employés                                        |
-- | Ouvriers                                        |
-- | Inactifs ayant déjà travaillé                   |
-- +-------------------------------------------------+
-- = Reponse question 4.

-- au moins 1 

select distinct intitulecat from CATEGORIE natural join CARACTERISTIQUE natural join SONDE where YEAR(datenaissond) = 1976;

-- +------------------+--
-- * Question 5 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--   Quels sont les sondés nés en 1998 qui appartiennent aux panels France global 1 et France global 2?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +------------+------------+
-- | nomSond    | prenomSond |
-- +------------+------------+
-- | TRISAULOLU | Elise      |
-- | MAMIAT     | Mathieu    |
-- | NEUSIL     | Theo       |
-- +------------+------------+
-- = Reponse question 5.

-- type au moins 2 

select distinct nomsond, prenomsond from SONDE natural join CONSTITUER where YEAR(datenaissond) = 1998 and numsond in (select numsond from CONSTITUER natural join PANEL where nompan = 'France Global 1' and numsond in (select numsond from CONSTITUER natural join PANEL where nompan = 'France Global 2'));

-- +------------------+--
-- * Question 6 :     --
-- +------------------+--
-- Ecrire une requête qui renvoie les informations suivantes:
--  Quels sont les sondés nés en 1976 qui ont la même date de naissance?

-- Voici le début de ce que vous devez obtenir.
-- ATTENTION à l'ordre des colonnes et leur nom!
-- +---------+------------+----------+------------+
-- | nomSond | prenomSond | nomSond  | prenomSond |
-- +---------+------------+----------+------------+
-- | DASA    | Maxime     | PEKARDAC | Bilal      |
-- +---------+------------+----------+------------+
-- = Reponse question 6.

--type au moin 2 (couple)

select distinct S1.nomsond, S1.prenomSond, S2.nomsond, S2.prenomsond from SONDE S1 inner join SONDE S2 on S1.datenaissond = S2.datenaissond where S1.datenaissond = S2.datenaissond and YEAR(S1.datenaissond) = 1976 and S1.numsond <> S2.numsond;