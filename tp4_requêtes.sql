/* Question 1-1 */

create or replace view Produit10 as select refProd, nomProd puProd from PRODUIT where puProd > 10; 

/* Question 1-2 */

create or replace view DixMars2015 as select refProd, nomProd, puProd from PRODUIT natural join DETAIL natural join FACTURE where dateFac = STR_TO_DATE('10/3/2015','%d/%m/%Y');   


/* Question 1-3 */

create or replace view NbClientsParVille as select adresseCli ville, count(distinct(numCli)) nbCli from CLIENT group by ville; 
select * from NbClientsParVille where SUBSTR(ville,1,1) = 'M';

/* Question 1-4 */

create or replace view CAParMois as select MONTH(dateFac) mois, YEAR(dateFac) annee, (refProd*puProd) CA from FACTURE natural join DETAIL natural join PRODUIT order by CA;

/* Question 1-5 */

drop view Produit10;
drop view DixMars2015;
drop view NbClientsParVille;
drop view CAParMois;

/* Question 2-1 */

select distinct nomProd from PRODUIT natural left join DETAIL where numFac is NULL;

/* Question 2-2 */

select nomProd, count(distinct(numFac)) nbFac from PRODUIT natural left join DETAIL where SUBSTR(nomProd,1,1)='M';

/* Question 2-3 */
