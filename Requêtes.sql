/* Exercice 2 - Question 1 */

select * from Caracteristique;
select * from Panel;
select * from Sonde;
select * from Tranche;
select * from Categorie;
select * from Constituer;

/* Exercice 2 - Question 2 */

select dateNaisSond from Sonde where nomSond = 'Anna SITOURIN';

select distinct idPan, nomPan from Sonde Natural Join Constituer Natural Join Panel where nomSond = 'Anna SITOURIN';

select distinct idCat, intituleCat from Sonde Natural Join Caracteristique Natural Join Categorie where nomSond = 'Anna SITOURIN';

select distinct valDebut, valFin from Sonde Natural Join Caracteristique Natural Join Tranche where nomSond = 'Anna SITOURIN';

/* Exercice 3 - Question 1 */

select distinct idPan, nomPan from Sonde Natural Join Constituer Natural Join Panel where nomSond = 'Caroline BOURIER';

select S1.idPan, S1.nomPan 
from Sonde S1 
where EXISTS (select * 
            from Sonde S1, Constituer C1, Panel P1 
            where S1.numSond = C1.numSond and C1.idPan = P1.idPan and nomSond = 'Caroline BOURIER');

select distinct S1.idPan, S1.nomPan from Sonde S1 where IN select * from Sonde, Constituer, Panel where  nomSond = 'Caroline BOURIER';

/* Exercice 3 - Question 2 */

select distinct idPan, nomPan from Sonde Natural Join Constituer Natural Join Panel where valDebut >= 70 and valFin <= 120;

select S1.idPan, S1.nomPan 
from Sonde S1 
where EXISTS (select * 
            from Sonde S1, Constituer C1, Panel P1 
            where S1.numSond = C1.numSond and C1.idPan = P1.idPan and valDebut >= 70 and valFin <= 120);

select distinct S1.idPan, S1.nomPan from Sonde S1 where IN select * from Sonde, Constituer, Panel where valDebut >= 70 and valFin <= 120;


/* Exercice 3 - Question 3 */

select distinct idPan, nomPan from Sonde Natural Join Constituer Natural Join Panel where valDebut >= 70 and valFin <= 120 and intituleCat = 'Ouvriers';

select S1.idPan, S1.nomPan 
from Sonde S1 
where EXISTS (select * 
            from Sonde S1, Constituer C1, Panel P1 
            where S1.numSond = C1.numSond and C1.idPan = P1.idPan and valDebut >= 70 and valFin <= 120 and intituleCat = 'Ouvriers');

select distinct S1.idPan, S1.nomPan from Sonde S1 where IN select * from Sonde, Constituer, Panel where valDebut >= 70 and valFin <= 120 and intituleCat = 'Ouvriers';

/* Exercice 3 - Question 4 */

select nomSond, PrenomSond from Sonde Natural Join Caracteristique Natural Join Categorie where PrenomSond = 'Olivier' and intituleCat = 'Ouvriers';

select S1.nomSond, S1.PrenomSond  
from Sonde S1 
where EXISTS (select * 
            from Sonde S1, Caracteristique C1, Categorie Ca1 
            where S1.idC = C1.idC and C1.idCat = Ca1.idCat and PrenomSond = 'Olivier' and intituleCat = 'Ouvriers');

select S1.nomSond, S1.PrenomSond from Sonde S1 where S1.PrenomSond IN select * from Sonde S1, Caracteristique C1, Categorie Ca1  where S1.idC = C1.idC and C1.idCat = Ca1.idCat and PrenomSond = 'Olivier' and intituleCat = 'Ouvriers');


/* Exercice 3 - Question 5 */

/* Avec les années et renvoyer l'année la plus petite et l'année la plus grande ?


/* Exercice 3 - Question 6 */

select S1.prenomSond, S1.nomSond
from Sonde S1, Constituer C1, Panel P1
where S1.numSond = C1.nomSond and C1.idPan = P1.idPan and prenomSond = 'Jean'
union 
select S2.PrenomSond, S2.nomSond
from Sonde S2, Constituer C2, Panel P2
where S2.numSond = C2.nomSond and C2.idPan = P2.idPan and P2.idPan <> P1.idPan and prenomSond = 'Jean'; 